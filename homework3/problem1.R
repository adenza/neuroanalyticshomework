
options(stringsAsFactors = FALSE);


#open pdf, to output to
pdf("longitudinaldata.pdf");

##plotting longitudinal data for each 100 donors
#10 time points, with 5 data points in each

##timepoints for plotting
tp = rep(1:10,each=5)

#loop for each 100
for (i in 1:100) {
  
  if (i<10) {
    donorfileid = paste0("donor00",i);
  } else if (i < 100) {
    donorfileid=paste0("donor0",i)
  } else if (i < 1000) {
    donoffileid = paste0("donor",i)
  }
  
  datamat = matrix(NA,nrow=5,ncol=10);
  
  #10 time points loop
  for (j in 1:10) {
    
    if (j<10) { 
      timepointfileid = paste0("tp0",j);
    } else if (j<100) {
      timepointfileid = paste0("tp",j)
    }
    
    txtfileid = paste0(donorfileid,"_",timepointfileid,".txt"); 
    output = read.table(paste0("/nas/longleaf/home/adenza/neuroanalytics/homework3/",txtfileid),header=TRUE);
    datamat[,j] = output$data;
    
    
  }
  #timepoint vs phenotype
  plot(tp,datamat,main=donorfileid,xlab="Time point",ylab="phenotype");
}





#save pdf
dev.off()