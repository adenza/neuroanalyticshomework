#!/bin/bash

#for loop repeats loop 10 times
for i in `seq 1 10`
do
#when the seq is less than 6, run counterclockwise
if (($i \< 6));
then
    # -n no newline character
    # -e interpret backslash escape 
    echo -ne '\\';
    #sleep waits one second before execution
    sleep .1;
    #\b move cursor back one character (thus able to overwrite)
    echo -ne '\b-';
    sleep .1;
    #\\ becomes an actual backslash, under the context that \ is escape char
    echo -ne '\b/'
    sleep .1;
    echo -ne '\b|';
    sleep .1;
    echo -ne '\b\\';
    sleep .1;
    echo -ne '\b-';
    sleep .1;
    echo -ne '\b/';
    sleep .1;
    echo -ne '\b|';
    #\n creates new line
    echo -ne "\b \n";
#else, run clockwise
else
    #show action worked
    sleep 2;
    # -n no newline character
    # -e interpret backslash escape 
    echo -ne '/';
    #sleep waits one second before execution
    sleep .1;
    #\b move cursor back one character (thus able to overwrite)
    echo -ne '\b-';
    sleep .1;
    #\\ becomes an actual backslash, under the context that \ is escape char
    echo -ne '\b\\'
    sleep .1;
    echo -ne '\b|';
    sleep .1;
    echo -ne '\b/';
    sleep .1;
    echo -ne '\b-';
    sleep .1;
    echo -ne '\b\\';
    sleep .1;
    echo -ne '\b|';
    #\n creates new line
    echo -ne "\b \n";
fi
done




