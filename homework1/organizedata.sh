#!/bin/bash

#prepare fakedata dir
#mkdir fakedata;
#for loop for running through files and moving them
for i in `seq 1 50`
do
    #makes the 50 donor directories
    mkdir donor${i};
    #for all files of this type
    for file in donor${i}_tp{1..10}.txt;
    do
	#move all donor files into their correct folders
       	mv donor${i}_tp{1..10}.txt pwd donor${i};
    done
    #move all dir to fakedata
	mv donor${i}/ fakedata/;
	#make all files read only
	
done

#--chmod -R a-r fakedata
