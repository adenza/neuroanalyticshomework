#!/bin/bash

#what file we are pointing at
testfile="hapmap1.ped"
#grabs number of lines in the file
line=`wc -l $testfile`
number=`basename $line`
echo $line
#for number of lines
for i in `seq 1 $number`;
do
    head -n $i $testfile | tail -n 1
    sleep 1;
done
